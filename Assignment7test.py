import pytest
import Assignment7


def test_linkedlist():
    assert 'insert 7 at beg',"The list: 7"

    assert 'insert 6 at beg',"The list: 6 7"

    assert 'insert 5 at end',"The list: 6 7 5"

    assert 'insert 4 after 1',"The list: 6 7 4 5"

    assert 'remove 0',"The list: 7 4 5"
