import unittest
import Assignment5main

class TestCal(unittest.TestCase):
    def test_add(self):
        self.assertEqual(Assignment5main.add(7,7),14)

    def test_add_notequal(self):
        self.assertNotEqual(Assignment5main.add(60,20),90)

    def test_add_True(self):
        self.assertTrue(Assignment5main.add(15,22)==37,"False")

    def test_add_False(self):
        self.assertFalse(Assignment5main.add(12,13)==2,"True")

    def test_add_Is(self):
        self.assertIs(Assignment5main.add(23,43),Assignment5main.add(23,43),"Do not Match")
    
    def test_add_IsNot(self):
        self.assertIsNot(Assignment5main.add(56,76),Assignment5main.add(56,78),"Match!")

    def test_add_IsNotNone(self):
        self.assertIsNotNone(Assignment5main.add(23,56),"None")
    
    def test_add_In(self):
        self.assertIn(Assignment5main.add(50,65),[113,114,115],"Not in list")

    def test_add_almostEqual(self):
        self.assertAlmostEqual(Assignment5main.add(45,2),47.000000001)

    def test_add_almostnotEqual(self):
        self.assertNotAlmostEqual(Assignment5main.add(12,13),25.0985001)

    def test_add_greater(self):
        self.assertGreater(Assignment5main.add(45,55),1)

if __name__=='__main__':
    unittest.main()
